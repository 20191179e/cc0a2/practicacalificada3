package pe.uni.maxwelparedesl.pacticacalificada3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {


    RadioButton buttonBasico;
    RadioButton buttonCientifico;
    RadioButton buttonProgramador;
    Button buttonEmpezar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonBasico = findViewById(R.id.radio_button_basico);
        buttonCientifico = findViewById(R.id.radio_button_cientifico);
        buttonProgramador = findViewById(R.id.radio_button_programador);
        buttonEmpezar = findViewById(R.id.button_empezar);

        buttonEmpezar.setOnClickListener(v->{


            if(buttonBasico.isChecked()){
                Intent intent = new Intent(MainActivity.this,CalculatorActivity.class);
                startActivity(intent);

            }

            if(buttonCientifico.isChecked()){
                AlertDialog.Builder builderDialg = new AlertDialog.Builder(MainActivity.this);
                builderDialg
                        .setTitle(R.string.dialog_smg_title)
                        .setMessage(R.string.dialog_smg)
                        .setPositiveButton(R.string.si,(d,w)->{

                         }).setNegativeButton(R.string.no,(d,w)->{
                            moveTaskToBack(true);

                            android.os.Process.killProcess(android.os.Process.myPid());

                            System.exit(1);
                }).create().show();
            }

            if(buttonProgramador.isChecked()){
                AlertDialog.Builder builderDialg = new AlertDialog.Builder(MainActivity.this);
                builderDialg
                        .setTitle(R.string.dialog_smg_title)
                        .setMessage(R.string.dialog_smg)
                        .setPositiveButton(R.string.si,(d,w)->{

                        }).setNegativeButton(R.string.no,(d,w)->{
                    moveTaskToBack(true);

                    android.os.Process.killProcess(android.os.Process.myPid());

                    System.exit(1);
                }).create().show();
            }

        });




    }
}