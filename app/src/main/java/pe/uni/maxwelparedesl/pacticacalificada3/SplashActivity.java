package pe.uni.maxwelparedesl.pacticacalificada3;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {

    ImageView imageViewSplash;
    TextView titleViewSplash;
    Animation animationImageSplash,animationTitleViewSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageViewSplash = findViewById(R.id.image_splash);
        titleViewSplash = findViewById(R.id.title_splash);

        animationImageSplash = AnimationUtils.loadAnimation(this, R.anim.portada_animation);

        imageViewSplash.setAnimation(animationImageSplash);

        new CountDownTimer(6000,1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intentSplash = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(intentSplash);
            }

        }.start();
    }
}