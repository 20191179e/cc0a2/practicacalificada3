package pe.uni.maxwelparedesl.pacticacalificada3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class CalculatorActivity extends AppCompatActivity {

    Button button_1, button_2, button_3, button_4, button_5, button_6, button_7,eliminar, button_8, button_9, button_0, calcular, mas, menos, multiplicar, dividir;
    TextView textViewNumero;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        button_0 = findViewById(R.id.button_0);
        button_1 = findViewById(R.id.button_1);
        button_2 = findViewById(R.id.button_2);
        button_3 = findViewById(R.id.button_3);
        button_4 = findViewById(R.id.button_4);
        button_5 = findViewById(R.id.button_5);
        button_6 = findViewById(R.id.button_6);
        button_7 = findViewById(R.id.button_7);
        button_8 = findViewById(R.id.button_8);
        button_9 = findViewById(R.id.button_9);
        calcular = findViewById(R.id.calcular);
        mas = findViewById(R.id.button_mas);
        menos =findViewById(R.id.button_menos);
        multiplicar = findViewById(R.id.button_multiplicar);
        dividir = findViewById(R.id.button_dividir);
        eliminar = findViewById(R.id.button_eliminar);
        textViewNumero = findViewById(R.id.text_number);
       button_0.setOnClickListener(v->{
           int numero_0 = 0;
           textViewNumero.setText(String.valueOf(numero_0));
       });
        button_1.setOnClickListener(v->{
            int numero_1 = 1;
            textViewNumero.setText(String.valueOf(numero_1));
        });
    }
}